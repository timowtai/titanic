import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    titles_to_consider = ["Mr.", "Mrs.", "Miss."]

    results = []

    for title in titles_to_consider:

        filtered_data = df[df['Name'].str.contains(title, case=False, na=False)]

        missing_values = filtered_data['Age'].isnull().sum()

        median_age = round(filtered_data['Age'].median())

        results.append((title, missing_values, median_age))

        return results
